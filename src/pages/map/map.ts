import { Component, ViewChild, ElementRef } from '@angular/core';

import { ConferenceData } from '../../providers/conference-data';

import { Platform } from 'ionic-angular';

import { IssuePage } from '../issue/issue';
import { UnwellPage } from '../unwell/unwell';
import { SecurityPage } from '../security/security';
import { LostProperty } from '../lostproperty/lostproperty';

import { NavController } from 'ionic-angular';


declare var google: any;


@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})
export class MapPage {


  //pages for the small button on the side

  public rootPage1: any = IssuePage;
  public rootPage2: any = UnwellPage;
  public rootPage3: any = SecurityPage;
  public rootPage4: any = LostProperty;


  @ViewChild('mapCanvas') mapElement: ElementRef;
  constructor(public confData: ConferenceData, public platform: Platform, public navCtrl: NavController) {
  }

  //small button methods to open pages
    openIssuePage(){
      console.log("111111")

        this.navCtrl.push(this.rootPage1);
    }

    openUnwellPage(){
      console.log("222222")

      this.navCtrl.push(this.rootPage2);

    }

    openSecurityPage(){
      console.log("3333")

      this.navCtrl.push(this.rootPage3);

    }

    openLostPropertyPage(){
      console.log("4444")

      this.navCtrl.push(this.rootPage4);

    }

}
