import { Component } from '@angular/core';

import { Platform } from 'ionic-angular';
import { IssueReportPage } from '../issuereport/issuereport';
import { NavController } from 'ionic-angular';


@Component({
  selector: 'page-issue',
  templateUrl: 'issue.html'
})
export class IssuePage {


  firstPassed: any;
  secondPassed: any;

  public newPage: any = IssueReportPage;

  constructor( public platform: Platform, public navCtrl: NavController) {
  }

  openReportPage(){
    console.log("report page opneed");
    this.navCtrl.push(this.newPage);
  }

  openBroken(){
    console.log("asasfasg");
    this.navCtrl.push(this.newPage,{

        firstPassed: "Broken/soiled train",
        secondPassed: "Table Item Subtitle Text"
    });

  }
  openSmell(){
    console.log("asasfasg");
    this.navCtrl.push(this.newPage,{

        firstPassed: "Smell",
        secondPassed: "Table Item Subtitle Text"
    });

  }
  openTemperature(){
    console.log("asasfasg");
    this.navCtrl.push(this.newPage,{

        firstPassed: "Temperature",
        secondPassed: "Table Item Subtitle Text"
    });

  }
  openNoise(){
    console.log("asasfasg");
    this.navCtrl.push(this.newPage,{

        firstPassed: "Noise",
        secondPassed: "Table Item Subtitle Text"
    });

  }
  openOther(){
    console.log("asasfasg");
    this.navCtrl.push(this.newPage,{
      firstPassed: "Other",
      secondPassed: "Table Item Subtitle Text"
    });

  }

}
