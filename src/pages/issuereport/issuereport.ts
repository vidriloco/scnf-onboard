import { Component } from '@angular/core';

import { Platform } from 'ionic-angular';
import { NavController, NavParams } from 'ionic-angular';




@Component({
  selector: 'page-issuereport',
  templateUrl: 'issuereport.html'
})
export class IssueReportPage {

  firstParam:any;
  secondParam:any;

  constructor( public platform: Platform, public navCtrl: NavController, public navParams: NavParams) {

    this.firstParam = navParams.get("firstPassed");
    this.secondParam = navParams.get("secondPassed");

  }


}
