import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { NavController,ToastController  } from 'ionic-angular';

import { UserData } from '../../providers/user-data';

import { UserOptions } from '../../interfaces/user-options';

import { MapPage } from '../map/map';
import { AlertController } from 'ionic-angular';
import { DataProvider } from '../../services/data_provider';
import { Storage } from '@ionic/storage';

import { AWScreate } from '../../libs/awsLibJs';

@Component({
  selector: 'page-user',
  templateUrl: 'signup.html'
})
export class SignupPage {
  signup: UserOptions = { pnr: ''};
  submitted = false;

  ticket_code: string;

  constructor(public navCtrl: NavController,
              public userData: UserData,
              public alertCtrl: AlertController,
              public dataProvider: DataProvider,
              public toastCtrl: ToastController,
              public storage: Storage)
    {
      this.firstTimeSetup();
      this.dataProvider.storage.get('setup').then(result => {
      console.log(result)
    });
  }

  onSignup(form: NgForm) {

    this.submitted = true;

    if (form.valid) {
      console.log("Print: ", form.value.username);

      this.userData.signup(form.value.username);

      // save to AWS
      var params = {
        TableName: "scnfJourneyInfo",
        Key: {
          "pnr": form.value.username
        },
        UpdateExpression: "set departureTime = :d, destination = :e, origin = :o",
        ExpressionAttributeValues:{
          ":d": Date.now()/1000 - 360000,
          ":e": "Paris",
          ":o": "Bordeaux"
        },
        ReturnValues:"UPDATED_NEW"
      }

      console.log("Updating the item...");
      var awsCreate = new AWScreate;
      var docClient = awsCreate.ddb()
      docClient.update(params, function(err, data) {
          if (err) {
              console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
          } else {
              console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
          }
      });

      this.saveData();
      this.presentConfirm();
    }
  }

    presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Important',
      message: 'Share your location? (we need this to help you while onboard the train)',
      buttons: [
        {
          text: 'Disagree',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {

            console.log('Agree clicked-> went to Map page');
            this.navCtrl.push(MapPage);

          }
        }
      ]
    });
    alert.present();
    }




    firstTimeSetup(){
    this.dataProvider.getFromStorage('setup',result => {
    if (result !='done'){
       this.dataProvider.setUpStorageLists();
                   this.settingUp();
        }
      });
    }

    settingUp() {
      let toast = this.toastCtrl.create({
        message: 'Performing First Time Setup',
        duration: 3000
      });
      toast.present();
    }






      /**
        USE THIS BELOW TO DELETE EVERYTHING FROM THE LOCAL DATABASE
      */


    /*
    clearStorage(){
      this.data_provider.storage.clear().then(() => {
        this.data_provider.storage.set('setup', false);
        this.data_provider.setUpStorageLists();
        console.log('storage is clear')
      })
    }*/



    saveData(){
      console.log("saved new entry")
      this.dataProvider.store('entry_data');
    }


}
