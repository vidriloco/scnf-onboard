import { Injectable } from '@angular/core';
import { Device } from '@ionic-native/device';
import { Platform } from 'ionic-angular';

//import { Http, Headers} from '@angular/http'; I forgot what this does, maybe not required?
import { Storage } from '@ionic/storage';

@Injectable()
export class DataProvider {

    ticketCode: string;

    constructor(public storage: Storage,
                public device: Device,
                public platform: Platform) {
                }



    getTicketCode(){
      return this.ticketCode;
    }

    setTicketCode(ticketCode: string){

      console.log("ticket is now saved");
      this.ticketCode=ticketCode;

    }


    setUpStorageLists() {
      this.storage.get('setup').then(result => {
        if (result != 'done'){
          this.storage.set('entry_data', '');
          this.storage.set('setup', 'done');
         }
      });
    }

        // gets the full list from storage
    getFromStorage(key: string, callback) {
      this.storage.get(key).then(val => {callback(val)});
    }



    store(key: string){
      let dataEntry = new Entry(this.ticketCode);
      console.log("Saving entry.... ");
      // Store the updated list
      this.storage.set(key, dataEntry).then(result => {
        return(result);
      });
    };
}


  export class Entry {

    ticketCode: string;

    constructor(ticketCode: string){
      this.ticketCode=ticketCode;


    }
  }
